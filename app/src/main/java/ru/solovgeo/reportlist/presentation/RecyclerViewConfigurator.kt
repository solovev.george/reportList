package ru.solovgeo.reportlist.presentation

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import ru.solovgeo.reportlist.presentation.base.BaseAdapter

class RecyclerViewConfigurator(
        val adapter: BaseAdapter,
        private var hasFixedSize: Boolean = false,
        private var layoutManagerProvider: LayoutManagerProvider = LinearLayoutManagerProvider()
) {

    fun applyConfig(recyclerView: RecyclerView) {
        recyclerView.layoutManager = layoutManagerProvider.get(recyclerView.context)
        recyclerView.setHasFixedSize(hasFixedSize)
        recyclerView.adapter = adapter
    }

    interface LayoutManagerProvider {
        operator fun get(context: Context): RecyclerView.LayoutManager
    }

    class LinearLayoutManagerProvider : LayoutManagerProvider {

        private var linearLayoutManager: LinearLayoutManager? = null

        override fun get(context: Context): RecyclerView.LayoutManager {
            if (linearLayoutManager == null) {
                linearLayoutManager = LinearLayoutManager(context)
            } else {
                val state = linearLayoutManager?.onSaveInstanceState()
                linearLayoutManager = LinearLayoutManager(context)
                linearLayoutManager?.onRestoreInstanceState(state)
            }
            return linearLayoutManager ?: LinearLayoutManager(context)
        }
    }
}

