package ru.solovgeo.reportlist.presentation

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.databinding.ObservableField
import android.view.View
import ru.solovgeo.reportlist.business.ReportInteractor
import ru.solovgeo.reportlist.other.ErrorHandler
import ru.solovgeo.reportlist.other.addTo
import ru.solovgeo.reportlist.presentation.base.BaseViewModel
import javax.inject.Inject


class MainViewModel(
        private val interactor: ReportInteractor,
        private val errorHandler: ErrorHandler,
        adapter: ReportListAdapter
) : BaseViewModel() {
    val title = ObservableField<String>()
    val subtitle = ObservableField<String>()
    val recyclerViewConfigurator = RecyclerViewConfigurator(adapter = adapter, hasFixedSize = true)
    val progressVisibility = ObservableField<Int>(View.VISIBLE)
    val layoutVisibility = ObservableField<Int>(View.GONE)
    val errorVisibility = ObservableField<Int>(View.GONE)
    val errorMessage = ObservableField<String>()

    init {
        getData()
    }

    fun getData() {
        interactor.getReportList()
                .subscribe(
                        {
                            progressVisibility.set(View.GONE)
                            errorVisibility.set(View.GONE)
                            layoutVisibility.set(View.VISIBLE)
                            title.set(it.title)
                            subtitle.set(it.subtitle)
                            recyclerViewConfigurator.adapter.addItems(it.viewModels)
                        },
                        {
                            progressVisibility.set(View.GONE)
                            layoutVisibility.set(View.GONE)
                            errorVisibility.set(View.VISIBLE)
                            errorHandler.proceed(it, { errorMessage.set(it) })
                        })
                .addTo(compositeDisposable)
    }


    class MainViewModelFactory @Inject constructor(
            private val interactor: ReportInteractor,
            private val errorHandler: ErrorHandler,
            private val adapter: ReportListAdapter
    ) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MainViewModel(interactor, errorHandler, adapter) as T
        }

    }
}