package ru.solovgeo.reportlist.presentation

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.solovgeo.reportlist.R
import ru.solovgeo.reportlist.databinding.ActivityMainBinding
import ru.solovgeo.reportlist.other.di.DI
import toothpick.Toothpick

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initVM(binding)
    }

    private fun initVM(binding: ActivityMainBinding) {
        val factory = Toothpick.openScopes(DI.APP_SCOPE).getInstance(MainViewModel.MainViewModelFactory::class.java)
        viewModel = ViewModelProviders
                .of(this, factory)
                .get(MainViewModel::class.java)
        binding.vm = viewModel
    }
}
