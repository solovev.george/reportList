package ru.solovgeo.reportlist.presentation.entity

import android.databinding.ObservableField

class ReportVM {

    val name = ObservableField<String>()
    val period = ObservableField<String>()
    val status = ObservableField<String>()
    val statusColor = ObservableField<Int>()

}