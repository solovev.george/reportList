package ru.solovgeo.reportlist.presentation

import ru.solovgeo.reportlist.BR
import ru.solovgeo.reportlist.R
import ru.solovgeo.reportlist.presentation.base.BaseAdapter
import ru.solovgeo.reportlist.presentation.entity.ReportMonthTitleVM
import ru.solovgeo.reportlist.presentation.entity.ReportVM
import ru.solovgeo.reportlist.presentation.entity.Separator
import javax.inject.Inject

class ReportListAdapter @Inject constructor() : BaseAdapter() {
    init {
        cell(ReportVM::class.java, R.layout.cell_report, BR.vm)
        cell(ReportMonthTitleVM::class.java, R.layout.cell_report_month_title, BR.vm)
        cell(Separator::class.java, R.layout.cell_separator, BR._all)
    }
}