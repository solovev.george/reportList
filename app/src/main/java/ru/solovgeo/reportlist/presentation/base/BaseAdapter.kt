package ru.solovgeo.reportlist.presentation.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*
import kotlin.collections.ArrayList

abstract class BaseAdapter : RecyclerView.Adapter<BaseAdapter.ViewHolder>() {

    private val viewModels = ArrayList<Any>()
    private val cellMap = HashMap<Class<out Any>, CellInfo>()
    private val sharedObjects = SparseArray<Any>()

    // Public functions:

    fun addItems(items: List<Any>) {
        val previousSize = this.viewModels.size
        this.viewModels.addAll(items)
        val itemsInserted = this.viewModels.size - previousSize
        notifyItemRangeInserted(previousSize, itemsInserted)
    }

    fun deleteAllItems() {
        val previousSize = this.viewModels.size
        this.viewModels.clear()
        notifyItemRangeRemoved(0, previousSize)
    }

    //Adapter initialization functions

    protected fun cell(clazz: Class<out Any>, @LayoutRes layoutId: Int, bindingId: Int) {
        cellMap[clazz] = CellInfo(layoutId, bindingId)
    }

    protected fun onClickListener(sharedObject: Any, bindingId: Int) {
        sharedObjects.put(bindingId, sharedObject)
    }

    // RecyclerView.Adapter

    override fun getItemCount(): Int = viewModels.size

    override fun getItemViewType(position: Int): Int {
        return getCellInfo(viewModels[position]).layoutId
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val view = inflater.inflate(viewType, parent, false)
        val viewHolder = ViewHolder(view)

        (0 until sharedObjects.size()).forEach {
            val key = sharedObjects.keyAt(it)
            val value = sharedObjects.get(key)
            viewHolder.binding.setVariable(key, value)
        }

        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        if (holder != null) {
            val viewModel = viewModels[position]
            val cellInfo = getCellInfo(viewModel)
            if (cellInfo.bindingId != 0)
                holder.binding.setVariable(cellInfo.bindingId, viewModel)
        }
    }

    //Private functions

    private fun getCellInfo(viewModel: Any): CellInfo = cellMap[viewModel.javaClass] ?:
            throw Exception("Cell info for class ${viewModel.javaClass.name} not found.")

    // Support classes:

    data class CellInfo(val layoutId: Int, val bindingId: Int)

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = DataBindingUtil.bind<ViewDataBinding>(view)
    }
}