package ru.solovgeo.reportlist.presentation.entity

import android.support.annotation.ColorRes

data class ReportInfo(
        val title: String,
        val subtitle: String,
        @ColorRes val status: Int,
        val viewModels: List<Any>
) {

}