package ru.solovgeo.reportlist.presentation.entity

import android.databinding.ObservableField
import ru.solovgeo.reportlist.R


class ReportMonthTitleVM() {

    val month = ObservableField<Int>()
    val reportsInMonthCount = ObservableField<Int>()

    fun setMonth(month: Int) {
        this.month.set(when (month) {
            0 -> R.string.in_january
            1 -> R.string.in_february
            2 -> R.string.in_march
            3 -> R.string.in_april
            4 -> R.string.in_may
            5 -> R.string.in_june
            6 -> R.string.in_july
            7 -> R.string.in_august
            8 -> R.string.in_september
            9 -> R.string.in_october
            10 -> R.string.in_november
            11 -> R.string.in_december
            else -> {
                throw Exception("Wrong month number")
            }
        })
    }

    fun setReportsInMonthCount(count: Int) {
        reportsInMonthCount.set(count)
    }

}