package ru.solovgeo.reportlist.data.entity

data class StatusEntity(
        val name: String,
        val statusId: Int
)