package ru.solovgeo.reportlist.data.mappers

import ru.solovgeo.reportlist.R
import ru.solovgeo.reportlist.data.entity.ResponseEntity
import ru.solovgeo.reportlist.presentation.entity.ReportInfo
import ru.solovgeo.reportlist.presentation.entity.ReportMonthTitleVM
import ru.solovgeo.reportlist.presentation.entity.ReportVM
import ru.solovgeo.reportlist.presentation.entity.Separator
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ReportMapper @Inject constructor() {
    fun transformIntoReportInfo(responseEntity: ResponseEntity): ReportInfo {

        val events = responseEntity.events
        Collections.sort(events)

        val reportsWithTitles = ArrayList<Any>()
        var previousDate: Date? = null
        var reportsInMonthCount = 1
        var lastReportTitleVM = ReportMonthTitleVM()
        reportsWithTitles.add(lastReportTitleVM)

        for (i in 0 until events.size) {
            val event = events[i]
            if (previousDate != null) {
                val instance = Calendar.getInstance()

                instance.time = event.date
                val currentMonth = instance.get(Calendar.MONTH)
                val currentYear = instance.get(Calendar.YEAR)

                instance.time = previousDate
                val previousMonth = instance.get(Calendar.MONTH)
                val previousYear = instance.get(Calendar.YEAR)

                if (currentMonth == previousMonth && currentYear == previousYear) {
                    reportsInMonthCount++
                    reportsWithTitles.add(Separator())
                } else {
                    lastReportTitleVM.setMonth(previousMonth)
                    lastReportTitleVM.setReportsInMonthCount(reportsInMonthCount)

                    reportsInMonthCount = 1
                    lastReportTitleVM = ReportMonthTitleVM()
                    reportsWithTitles.add(lastReportTitleVM)
                }

                if (i == events.size - 1) {
                    lastReportTitleVM.setMonth(previousMonth)
                    lastReportTitleVM.setReportsInMonthCount(reportsInMonthCount)
                }
            }

            val report = ReportVM()
            report.name.set(event.name)
            report.period.set(event.period)
            report.status.set(event.status.name)
            val status = if (responseEntity.organization.status.statusId == 2) {
                R.color.dark_green
            } else {
                R.color.red
            }
            report.statusColor.set(status)
            reportsWithTitles.add(report)
            previousDate = event.date
        }

        val status = if (responseEntity.organization.status.statusId == 0) {
            R.color.dark_green
        } else {
            R.color.red
        }
        return ReportInfo(responseEntity.organization.name,
                responseEntity.organization.status.name,
                status,
                reportsWithTitles)
    }
}