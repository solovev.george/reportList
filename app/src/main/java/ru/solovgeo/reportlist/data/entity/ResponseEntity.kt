package ru.solovgeo.reportlist.data.entity

data class ResponseEntity(
        val organization: OrganizationEntity,
        val events: List<EventEntity>
)