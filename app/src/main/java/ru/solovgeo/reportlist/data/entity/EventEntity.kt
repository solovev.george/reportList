package ru.solovgeo.reportlist.data.entity

import java.util.*

data class EventEntity(
        val name: String,
        val date: Date, //2017-06-01T00:00:00.0000000+05:00
        val period: String,
        val status: StatusEntity
) : Comparable<EventEntity> {

    override fun compareTo(other: EventEntity): Int {
        return date.compareTo(other.date) * -1
    }
}