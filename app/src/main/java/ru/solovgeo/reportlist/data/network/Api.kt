package ru.solovgeo.reportlist.data.network

import io.reactivex.Single
import retrofit2.http.GET
import ru.solovgeo.reportlist.data.entity.ResponseEntity

interface Api {

    @GET("media/img/ob-director.json")
    fun getReportList(): Single<ResponseEntity>
}