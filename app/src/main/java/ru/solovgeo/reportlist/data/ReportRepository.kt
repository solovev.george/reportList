package ru.solovgeo.reportlist.data

import ru.solovgeo.reportlist.data.mappers.ReportMapper
import ru.solovgeo.reportlist.data.network.Api
import ru.solovgeo.reportlist.other.system.SchedulersProvider
import javax.inject.Inject

class ReportRepository @Inject constructor(
        private val schedulers: SchedulersProvider,
        private val reportMapper: ReportMapper,
        private val api: Api
) {
    fun getReportList() = api.getReportList().map(reportMapper::transformIntoReportInfo).subscribeOn(schedulers.io())
}