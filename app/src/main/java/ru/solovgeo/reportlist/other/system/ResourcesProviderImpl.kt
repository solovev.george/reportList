package ru.solovgeo.reportlist.other.system

import android.content.Context
import android.support.annotation.StringRes

class ResourcesProviderImpl(val context: Context) : ResourcesProvider {

    override fun getString(@StringRes resId: Int): String = context.getString(resId)

}