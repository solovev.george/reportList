package ru.solovgeo.reportlist.other

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import ru.solovgeo.reportlist.R
import ru.solovgeo.reportlist.other.system.ResourcesProvider
import java.io.IOException

fun Disposable.addTo(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}

fun Throwable.userMessage(resourceProvider: ResourcesProvider) = when (this) {
    is HttpException -> when (this.code()) {
        400 -> resourceProvider.getString(R.string.bad_request_error)
        401 -> resourceProvider.getString(R.string.unauthorized_error)
        403 -> resourceProvider.getString(R.string.forbidden_error)
        404 -> resourceProvider.getString(R.string.not_found_error)
        405 -> resourceProvider.getString(R.string.method_not_allowed_error)
        409 -> resourceProvider.getString(R.string.conflict_error)
        422 -> resourceProvider.getString(R.string.unprocessable_error)
        500 -> resourceProvider.getString(R.string.server_error_error)
        else -> resourceProvider.getString(R.string.unknown_error)
    }
    is IOException -> resourceProvider.getString(R.string.network_error)
    else -> resourceProvider.getString(R.string.unknown_error)
}