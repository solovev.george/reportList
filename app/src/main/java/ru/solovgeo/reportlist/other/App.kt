package ru.solovgeo.reportlist.other

import android.app.Application
import ru.solovgeo.reportlist.BuildConfig
import ru.solovgeo.reportlist.other.di.DI
import ru.solovgeo.reportlist.other.di.module.AppModule
import toothpick.Toothpick
import toothpick.configuration.Configuration
import toothpick.registries.FactoryRegistryLocator
import toothpick.registries.MemberInjectorRegistryLocator

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initAppScope()
        initToothpick()
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction().disableReflection())
            FactoryRegistryLocator.setRootRegistry(ru.solovgeo.reportlist.FactoryRegistry())
            MemberInjectorRegistryLocator.setRootRegistry(ru.solovgeo.reportlist.MemberInjectorRegistry())
        }
    }

    private fun initAppScope() {
        val appScope = Toothpick.openScopes(DI.APP_SCOPE)
        appScope.installModules(AppModule(this))
    }
}