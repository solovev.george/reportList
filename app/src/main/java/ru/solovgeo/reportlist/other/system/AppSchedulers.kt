package ru.solovgeo.reportlist.other.system

import io.reactivex.schedulers.Schedulers

class AppSchedulers : SchedulersProvider {
    override fun computation() = Schedulers.computation()
    override fun trampoline() = Schedulers.trampoline()
    override fun newThread() = Schedulers.newThread()
    override fun io() = Schedulers.io()
}