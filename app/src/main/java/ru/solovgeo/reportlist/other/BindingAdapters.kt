package ru.solovgeo.reportlist.other

import android.annotation.SuppressLint
import android.databinding.BindingAdapter
import android.support.annotation.StringRes
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import ru.solovgeo.reportlist.R
import ru.solovgeo.reportlist.presentation.RecyclerViewConfigurator

@BindingAdapter("configurator")
fun bindRecyclerViewAdapter(recyclerView: RecyclerView, configurator: RecyclerViewConfigurator) {
    configurator.applyConfig(recyclerView)
}

@SuppressLint("SetTextI18n")
@BindingAdapter("monthName", "reportsInMonthCount")
fun setMonthTitleText(textView: TextView, @StringRes monthName: Int, reportsInMonthCount: Int) {
    textView.text = textView.context.resources.getQuantityString(R.plurals.reports_were_submitted, reportsInMonthCount, reportsInMonthCount, textView.context.getString(monthName))
}