package ru.solovgeo.reportlist.other.system

import io.reactivex.Scheduler

interface SchedulersProvider {
    fun computation(): Scheduler
    fun trampoline(): Scheduler
    fun newThread(): Scheduler
    fun io(): Scheduler
}