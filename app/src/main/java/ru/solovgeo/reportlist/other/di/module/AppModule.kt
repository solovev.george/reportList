package ru.solovgeo.reportlist.other.di.module

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import ru.solovgeo.reportlist.data.network.Api
import ru.solovgeo.reportlist.other.di.provider.ApiProvider
import ru.solovgeo.reportlist.other.di.provider.OkHttpClientProvider
import ru.solovgeo.reportlist.other.di.qualifier.ServerPath
import ru.solovgeo.reportlist.other.system.AppSchedulers
import ru.solovgeo.reportlist.other.system.ResourcesProvider
import ru.solovgeo.reportlist.other.system.ResourcesProviderImpl
import ru.solovgeo.reportlist.other.system.SchedulersProvider
import toothpick.config.Module

class AppModule(context: Context) : Module() {
    private val SERVER_URL = "http://urdm.ru/"

    init {
        //Global
        bind(Context::class.java).toInstance(context)
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())
        bind(ResourcesProvider::class.java).toInstance(ResourcesProviderImpl(context))

        //Network
        bind(String::class.java).withName(ServerPath::class.java).toInstance(SERVER_URL)
        bind(Gson::class.java).toInstance(GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").setLenient().create())
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java).singletonInScope()
        bind(Api::class.java).toProvider(ApiProvider::class.java).singletonInScope()
    }
}