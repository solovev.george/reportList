package ru.solovgeo.reportlist.other.system

interface ResourcesProvider {
    fun getString(resId: Int): String
}