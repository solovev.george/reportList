package ru.solovgeo.reportlist.other


import ru.solovgeo.reportlist.other.system.ResourcesProvider
import javax.inject.Inject

class ErrorHandler @Inject constructor(
        private val resourcesProvider: ResourcesProvider
) {
    fun proceed(error: Throwable, messageListener: (String) -> Unit = {}) {
        error.printStackTrace()
        messageListener(error.userMessage(resourcesProvider))
    }

}
