package ru.solovgeo.reportlist.other.di.provider

import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.solovgeo.reportlist.data.network.Api
import ru.solovgeo.reportlist.other.di.qualifier.ServerPath
import javax.inject.Inject
import javax.inject.Provider


class ApiProvider @Inject constructor(
        private val okHttpClient: OkHttpClient,
        @ServerPath private val serverPath: String,
        private val gson: Gson
) : Provider<Api> {

    override fun get() =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(serverPath)
                    .build()
                    .create(Api::class.java)
}