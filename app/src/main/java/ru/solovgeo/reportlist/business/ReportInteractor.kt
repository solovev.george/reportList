package ru.solovgeo.reportlist.business

import ru.solovgeo.reportlist.data.ReportRepository
import javax.inject.Inject

class ReportInteractor @Inject constructor(
        private val reportRepository: ReportRepository
) {
    fun getReportList() = reportRepository.getReportList()
}